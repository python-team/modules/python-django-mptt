python-django-mptt (0.13.2-2) unstable; urgency=medium

  * Team upload.
  * python-django-mptt-doc: add Breaks: and Replaces: on older versions or
    python3-django-mptt, as some files were moved from the later to the former
    in the last upload (fixing a bug, but introducing another one when
    upgrading).

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 30 Aug 2021 20:12:02 -0300

python-django-mptt (0.13.2-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-django-mptt-doc: Add Multi-Arch: foreign.

  [ Carsten Schoenert ]
  * [7d78df5] d/gbp.conf: Add some more defaults
  * [9d74c21] New upstream version 0.13.2
  * [3d89083] Add patches from patch queue branch
    Added patches:
    models-Adjust-msg-Title-underline-to-short.patch
    tutorial-Remove-unknown-sphinx-directive-python.patch
  * [ae5becf] d/control: Add new B-D on python3-sphinx-rtd-theme
  * [b9e6089] python-django-mptt-doc: Use dh_sphinxdoc for build
  * [a11009c] d/control: Order binary packages alphabetical
  * [ae61a4a] d/control: Remove Breaks for obsolete binary package
    The binary package python-django-mptt is now only existent in
    oldoldoldstable (stretch).
  * [3ef201b] d/control: Compressing the (Build-)Depends fields
  * [f2cdad6] d/control: Increase to Django version >=2 while built
  * [ebdb1da] d/control: Update Standards-Version to 4.6.0
    No further changes needed.
  * [b3aad95] d/watch: Update to version 4
  * [f144016] d/control: Adding entry Rules-Requires-Root: no

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 28 Aug 2021 09:47:35 +0200

python-django-mptt (0.11.0-1) unstable; urgency=medium

  * Team upload

  [ Antonio Terceiro ]
  * New upstream version 0.11.0
    - Builds and tests fine against Django 3 (Closes: #961168)
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Bump debhelper compatibility level to 13. This does not change the
    binaries produced.

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 23 May 2020 09:22:44 -0300

python-django-mptt (0.10.0-1) unstable; urgency=medium

  [ Antonio Terceiro ]
  * Team upload
  * New upstream version 0.10.0 (Closes: #933143)
  * Drop debian/patches/0001-Skip-doctest-test.patch, not needed anymore
  * Add new dependency: python3-django-js-asset
  * Bump Standards-Version; no changes needed otherwise

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 17 Aug 2019 13:40:17 -0300

python-django-mptt (0.8.7-1) unstable; urgency=medium

  * Team upload.

  [ Brian May ]
  * New upstream version.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces

  [ Chris Lamb ]
  * Remove trailing whitespaces.

  [ Antonio Terceiro ]
  * debian/watch: pull tarball from github
    - the releases uploaded to pypi do not containt the tests directory
  * Import a new copy of the 0.8.7, which includes the tests directory
    (now really Closes: #828669)
  * skip failing test
  * Run test suite under autopkgtest

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 12 Apr 2019 19:50:05 -0300

python-django-mptt (0.8.5-0.1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add patch to fix FTBFS in Django 1.10 (Closes: #828669):
    0001-django-1.10-Prepare-the-manager-using-code-for-Djang.patch

 -- Thomas Goirand <zigo@debian.org>  Fri, 05 Aug 2016 08:44:18 +0000

python-django-mptt (0.8.3-1) unstable; urgency=medium

  * New upstream version.

 -- Brian May <bam@debian.org>  Wed, 06 Apr 2016 13:35:31 +1000

python-django-mptt (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixes FTBFS error. Closes: #805688.
  * Maintainer not responding to emails. Setting maintainer to DPMT.

 -- Brian May <bam@debian.org>  Sun, 03 Jan 2016 15:22:59 +1100

python-django-mptt (0.7.4-1) unstable; urgency=medium

  * New upstream release.

 -- Brian May <bam@debian.org>  Mon, 30 Nov 2015 19:48:27 +1100

python-django-mptt (0.7.3-1) unstable; urgency=medium

  * New upstream release.

 -- Brian May <bam@debian.org>  Mon, 11 May 2015 15:15:09 +1000

python-django-mptt (0.6.1-1) unstable; urgency=medium

  * New upstream version.
  * Add python package.
  * Split documentation into -doc package.

 -- Brian May <bam@debian.org>  Wed, 04 Feb 2015 09:33:57 +1100

python-django-mptt (0.6.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Remove DM-Upload-Allowed; it's no longer used by the archive
    software.

  [ Janos Guljas ]
  * New upstream releaseo. (Closes: #719353)
  * Raise debconf comaptibility to 9.
  * Change my email address.
  * debian/control:
    - Raise standards to 3.9.4.
    - Remove XS-Python-Version and XB-Python-Version.
    - Build-Depend on debhelper (>= 9).
  * debian/copyright:
    - Update copyright years.
    - Update Format URL.

 -- Janos Guljas <janos@debian.org>  Thu, 15 Aug 2013 21:39:56 +0200

python-django-mptt (0.5.2-1) unstable; urgency=low

  * New upstream release.
  * Add python-all to build-depends.

 -- Janos Guljas <janos@resenje.org>  Sun, 11 Dec 2011 01:43:53 +0100

python-django-mptt (0.5.1-1) unstable; urgency=low

  * New upstream release.
  * Add debian/local-options.
  * Bump standards to 3.9.2.
  * Add debian/links and improve documentation packaging.
  * Fix dep5 syntax.
  * Add DM-Upload-Allowed control field.

 -- Janos Guljas <janos@resenje.org>  Fri, 18 Nov 2011 16:12:01 +0100

python-django-mptt (0.4.2-1) unstable; urgency=low

  * Initial release. (Closes: #611049)

 -- Janos Guljas <janos@resenje.org>  Sun, 06 Feb 2011 13:44:00 +0100
