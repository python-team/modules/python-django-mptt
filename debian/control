Source: python-django-mptt
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Brian May <bam@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-django (>= 2),
 python3-django-js-asset,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: https://github.com/django-mptt/django-mptt
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-mptt.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-mptt

Package: python-django-mptt-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Breaks: python3-django-mptt (<< 0.13.2-1~)
Replaces: python3-django-mptt (<< 0.13.2-1~)
Multi-Arch: foreign
Description: Modified Preorder Tree Traversal Django application (documentation)
 Django MPTT is a reusable/standalone Django application which aims to
 make it easy for you to use Modified Preorder Tree Traversal with your
 own Django models in your own applications.
 .
 It takes care of the details of managing a database table as a tree
 structure and provides tools for working with trees of model instances.
 .
 This package contains the documentation.

Package: python3-django-mptt
Architecture: all
Suggests: python-django-mptt-doc
Depends:
 libjs-jquery,
 libjs-underscore,
 python3-django,
 python3-django-js-asset,
 ${misc:Depends},
 ${python3:Depends}
Provides: ${python3:Provides}
Description: Modified Preorder Tree Traversal Django application
 Django MPTT is a reusable/standalone Django application which aims to
 make it easy for you to use Modified Preorder Tree Traversal with your
 own Django models in your own applications.
 .
 It takes care of the details of managing a database table as a tree
 structure and provides tools for working with trees of model instances.
